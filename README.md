# Prosjekt 2 - IMT2291 - Våren 2019 #

Rediger readme filen og legg inn navnene på deltakerne på gruppen før dere begynner med noe annet :-).

Beskrivelsen til oppgaven finnes i [Wikien til det opprinnelige prosjektet](https://bitbucket.org/okolloen/imt2291-prosjekt2-2019/wiki/Home).

Bruk Wikien til deres eget prosjekt til å dokumentere prosjektet slik det er beskrevet i oppgaven.